export enum Gender {
  male = 'M',
  female = 'F'
}

export class Person {
  id: string;
  name: string;
  lastname: string;
  age: number;
  gender: Gender;
  description: string;
  active: boolean;
}


