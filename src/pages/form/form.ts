import { Component } from '@angular/core';
import {AlertController, IonicPage, LoadingController, NavController, NavParams, ViewController} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Gender, Person} from "../../models/person";
import {FirebaseProvider} from "../../providers/firebase/firebase";

@IonicPage()
@Component({
  selector: 'page-form',
  templateUrl: 'form.html',
})
export class FormPage {
  genders = Gender;
  form: FormGroup;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public viewCtrl: ViewController,
    public firebase: FirebaseProvider,
    public alertCtrl: AlertController,
    public loadCtrl: LoadingController
  ) {

    this.form = this.formBuilder.group({
      name: ['', Validators.required],
      lastname: this.formBuilder.group({
        first: '',
        second: ''
      }),
      age: '',
      gender: '',
      description: '',
      active: false,
    });

    let person: Person = this.navParams.get('person');
    if (person) {
      this.form.patchValue({
        ...person
      })
    }
  }

  save() {
    const loading = this.loadCtrl.create({content: ''});
    loading.present();
    if (this.form.valid) {
      const person: Person = this.navParams.get('person');
      if (person) {
        this.firebase.put(person.id, {...this.form.value})
          .subscribe(
            (result) => this.onSuccess(result, true),
            (error) => this.onError(error,loading),
            () => loading.dismiss()
          )
      }
      else {
        this.firebase.post({...this.form.value})
          .subscribe(
            (result) => this.onSuccess(result, false),
            (error) => this.onError(error,loading),
            () => loading.dismiss()
      )
      }
    }
  }

  onSuccess = (person: Person, isUpdate) => {
    this.viewCtrl.dismiss({person, isUpdate});
  };
  onError = (error: any,loading) => {
    loading.dismiss();
    this.alertCtrl.create({
      title: 'Error',
      message: 'Http Error',
      buttons: [
        {text: 'ok', handler: ()=>{}}
      ]
    }).present()
  }






}
