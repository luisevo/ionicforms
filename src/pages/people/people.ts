import { Component } from '@angular/core';
import {AlertController, IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {Person} from "../../models/person";
import {FirebaseProvider} from "../../providers/firebase/firebase";

@IonicPage()
@Component({
  selector: 'page-people',
  templateUrl: 'people.html',
})
export class PeoplePage {

  people: Person[] = [];

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public modalCtrl: ModalController,
              public alertCtrl: AlertController,
              public firebase: FirebaseProvider) {
  }

  ionViewDidLoad() {
    this.load()
  }

  load(event?){

    this.firebase.get()
      .subscribe((result: Person[]) => {
        if (event) event.complete();
        this.people = result
      })
  }

  goForm(person?: Person) {
    const modal = this.modalCtrl.create('FormPage', {person});
    modal.present();
    modal.onDidDismiss((data: {person, isUpdate}) => {
      if (data) {
        if (data.isUpdate) {
          this.people = this.people.map(item => {
            if (item.id === data.person.id) return data.person
            return item
          })
        } else {
          this.people.push(data.person)
        }
      }
    })
  }

  remove(id) {
    const alert = this.alertCtrl.create({
      title: 'Confirmar',
      message: 'Desea eliminar esto?',
      buttons: [
        {
          text: 'Cancel',
          handler: ()=> {}},
        {
          text: 'Ok',
          handler: ()=> {
            this.firebase.remove(id)
              .subscribe(
                result => {
                  const index = this.people.findIndex(item => (item.id === id));
                  this.people.splice(index, 1)
                }
              )

          }},
      ]
    });
    alert.present()
  }

}
