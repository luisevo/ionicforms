import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators'
import {Observable} from "rxjs";
import {Person} from "../../models/person";

@Injectable()
export class FirebaseProvider {

  urlBase = 'https://ionicapp-7a398.firebaseio.com/';

  constructor(public http: HttpClient) {
  }

  get(): Observable<Person[]> {
    return this.http.get(`${this.urlBase}people.json`)
      .pipe(map( (result) => Object.keys(result).map(key => ({id: key, ...result[key]} as Person)))
      )
  }

  post(body) {
    return this.http.post(`${this.urlBase}people.json`, {...body})
      .pipe(map( (result: any) => ({id: result.name, ...body} as Person) ))
  }

  put(id, body) {
    return this.http.put(`${this.urlBase}people/${id}.json`, {...body})
      .pipe(map( result => ({id, ...result} as Person) ))
  }

  remove(id) {
    return this.http.delete(`${this.urlBase}people/${id}.json`)
  }


}
